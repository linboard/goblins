#lang racket/base

;;; Copyright 2020 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(provide swappable)

(require "cell.rkt"
         "methods.rkt"
         "../core.rkt"
         "../utils/values-to-list.rkt"
         (submod "../core.rkt" local-object-refr-debug-name)
         racket/contract)

(define (swappable initial-target
                   [proxy-name
                    (string->symbol
                     (format "swappable: ~a"
                             (local-object-refr-debug-name initial-target)))])
  (define-cell target
    initial-target)

  (define (^proxy _bcom)
    (make-keyword-procedure
     (lambda (kws kw-vals . args)
       (keyword-apply $ kws kw-vals ($ target) args))))

  (define proxy
    (spawn (procedure-rename ^proxy proxy-name)))

  (define/contract (swap new-target)
    (-> local-refr? any/c)
    ($ target new-target))

  (values proxy swap))

(module+ test
  (require rackunit
           racket/match)

  (define am
    (make-actormap))

  (define alice
    (actormap-spawn! am (lambda _ (lambda _ 'i-am-alice))))
  (define bob
    (actormap-spawn! am (lambda _ (lambda _ 'i-am-bob))))
  
  (match-define (list proxy-friend swap)
    (actormap-run! am (lambda () (values->list (swappable alice)))))

  (test-equal?
   "swappable proxy defaults to first entity"
   (actormap-peek am proxy-friend)
   'i-am-alice)
  
  (actormap-run! am (lambda () (swap bob)))

  (test-equal?
   "swappable proxy swaps"
   (actormap-peek am proxy-friend)
   'i-am-bob))
