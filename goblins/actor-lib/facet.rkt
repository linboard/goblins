#lang racket/base

;;; Copyright 2019-2021 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(require racket/contract
         racket/set
         racket/match
         "../core.rkt")

(provide ^facet facet)

(define (^facet bcom wrap-me
                #:async? [async? #f]
                . methods)
  (define method-set (apply seteq methods))
  (define $/<- (if async? <- $))
  (make-keyword-procedure
   (lambda (kws kw-args . args)
     (match args
       [(cons (? symbol? method) args)
        (unless (set-member? method-set method)
          (error (format "Access to method ~a denied" method)))
        (keyword-apply $/<- kws kw-args wrap-me method args)]
       [_ "Requires symbol-based method dispatch"]))))

(define (facet wrap-me
               #:async? [async? #f]
               . methods)
  (apply spawn (procedure-rename ^facet (object-name wrap-me))
         #:async? async? methods))

(module+ test
  (require rackunit
           "../core.rkt")
  (define am (make-actormap))
  (define (^wizard bcom)
    (match-lambda*
     [(list 'magic-missile level)
      (format "Casts magic missile level ~a!"
              level)]
     [(list 'flame-tongue level)
      (format "Casts flame tongue level ~a!"
              level)]
     [(list 'world-ender level)
      (format "Casts world ender level ~a!"
              level)]))
  (define all-powerful-wizard
    (actormap-spawn! am ^wizard))
  (define faceted-wizard
    (actormap-spawn! am ^facet all-powerful-wizard
                     'magic-missile
                     'flame-tongue))
  (check-equal?
   (actormap-peek am faceted-wizard 'magic-missile 2)
   "Casts magic missile level 2!")
  (check-equal?
   (actormap-peek am faceted-wizard 'flame-tongue 3)
   "Casts flame tongue level 3!")
  (check-exn
   any/c
   (lambda ()
     (actormap-peek am faceted-wizard 'world-ender 99))))
