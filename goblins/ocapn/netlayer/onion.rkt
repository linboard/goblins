#lang racket/base

;;; Copyright 2021 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(require "../../core.rkt"
         (submod "../../core.rkt" for-captp)
         "../../vat.rkt"
         "../../actor-lib/common.rkt"
         "../../actor-lib/methods.rkt"
         "../structs-urls.rkt"
         "onion-socks.rkt"
         "utils/read-write-procs.rkt"
         racket/match
         racket/unix-socket
         racket/async-channel
         racket/string
         racket/contract
         racket/file)

(provide restore-onion-netlayer
         new-onion-netlayer)

(define/contract (line-delimited-ports->channels ip op)
  (->* (input-port? output-port?) ()
       (values async-channel? async-channel?))
  (define in-ch (make-async-channel))
  (define out-ch (make-async-channel))

  (thread
   (lambda ()
     (let lp ([buf '()])
       (define (read-next-byte ip)
         (match (read-char ip)
           [(? eof-object?) 'done]
           [#\newline
            ;; Reverse and send to input channel current string
            (define incoming-str
              (string-trim (list->string (reverse buf)) "\r"
                           #:right? #t #:left? #f))
            (async-channel-put in-ch incoming-str)
            (lp '())]  ; safe to recur, handle-event is called in tail position
           ;; keep on bufferin'
           [char (lp (cons char buf))]))
       (sync
        ;; if it's closed, we're done
        (choice-evt (port-closed-evt ip)
                    (handle-evt ip read-next-byte))))))

  (thread
   (lambda ()
     (let lp ()
       (match (async-channel-get out-ch)
         ;; we're done
         ['close
          (close-input-port ip)
          (close-output-port op)]
         [msg
          (display msg op)
          (display "\r\n" op)
          (flush-output op)
          (lp)]))))

  (values in-ch out-ch))

(define (tor-control-connect-unix path)
  (define-values (tor-in-ch tor-out-ch)
    (unix-socket-connect path))
  (line-delimited-ports->channels tor-in-ch tor-out-ch))

(define default-goblins-tor-dir
  (build-path (find-system-path 'home-dir) ".cache" "goblins" "tor"))

(define default-tor-socks-path
  (build-path default-goblins-tor-dir "tor-socks-sock"))

(define default-tor-control-path
  (build-path default-goblins-tor-dir "tor-control-sock"))

(define default-tor-ocapn-socks-dir
  (build-path default-goblins-tor-dir "ocapn-sockets"))

(define (expect-250-ok tor-in-ch)
  (match (async-channel-get tor-in-ch)
    ["250 OK" 'ok]
    [something-else
     (error 'wrong-response "Failed to authenticate, got: ~a" something-else)]))

(define (setup-ocapn-io tor-control-path tor-ocapn-socks-dir)
  ;; Set up the temporary directory and paths we'll be using for this
  ;; captp process
  (make-directory* tor-ocapn-socks-dir)
  (define ocapn-sock-dir
    (make-temporary-file "ocapn-tor-~a" 'directory tor-ocapn-socks-dir))
  (define ocapn-sock-path
    (build-path ocapn-sock-dir "ocapn.sock"))

  (define-values (tor-in-ch tor-out-ch)
    (tor-control-connect-unix tor-control-path))
  
  (async-channel-put tor-out-ch "AUTHENTICATE")
  (expect-250-ok tor-in-ch)

  (define ocapn-sock-listener (unix-socket-listen ocapn-sock-path))
  (values ocapn-sock-dir ocapn-sock-path ocapn-sock-listener
          tor-in-ch tor-out-ch))

(define (new-tor-connection tor-control-path tor-ocapn-socks-dir)
  (define-values (ocapn-sock-dir
                  ocapn-sock-path ocapn-sock-listener
                  tor-in-ch tor-out-ch)
    (setup-ocapn-io tor-control-path tor-ocapn-socks-dir))

  (async-channel-put tor-out-ch
                     (format "ADD_ONION NEW:ED25519-V3 PORT=9045,unix:~a"
                             (path->string ocapn-sock-path)))

  (define service-id
    (let ([response (async-channel-get tor-in-ch)])
      (match (regexp-match (regexp "^250-ServiceID=(.+)$") response)
        [(list _ service-id) service-id]
        [#f (error 'wrong-response "Expected ServiceId response, got ~a"
                   response)])))
  (define private-key
    (let ([response (async-channel-get tor-in-ch)])
      (match (regexp-match (regexp "^250-PrivateKey=(.+)$") response)
        [(list _ service-id) service-id]
        [#f (error 'wrong-response "Expected PrivateKey response, got ~a"
                   response)])))

  (expect-250-ok tor-in-ch)
  
  (values ocapn-sock-dir ocapn-sock-path ocapn-sock-listener service-id private-key))

(define (restore-tor-connection tor-control-path tor-ocapn-socks-dir
                                private-key service-id)
  (define-values (ocapn-sock-dir
                  ocapn-sock-path ocapn-sock-listener
                  tor-in-ch tor-out-ch)
    (setup-ocapn-io tor-control-path tor-ocapn-socks-dir))
  (async-channel-put tor-out-ch
                     (format "ADD_ONION ~a PORT=9045,unix:~a"
                             private-key
                             (path->string ocapn-sock-path)))
  (define returned-service-id
    (let ([response (async-channel-get tor-in-ch)])
      (match (regexp-match (regexp "^250-ServiceID=(.+)$") response)
        [(list _ service-id) service-id]
        [#f (error 'wrong-response "Expected ServiceId response, got ~a"
                   response)])))
  (unless (equal? service-id returned-service-id)
    (error "Got the wrong service-id; expected ~a but got ~a"
           service-id returned-service-id))
  (expect-250-ok tor-in-ch)

  (values ocapn-sock-dir ocapn-sock-path ocapn-sock-listener))

(define (^onion-netlayer bcom our-location ocapn-sock-listener
                         tor-socks-path do-cleanup)
  (define shutdown-time (make-semaphore))
  (define (start-listen-thread conn-establisher)
    (define (handle-ocapn-sock-listen)
      (define-values (ip op)
        (unix-socket-accept ocapn-sock-listener))
      (define-values (read-message write-message)
        (read-write-procs ip op))
      (<-np-extern conn-establisher read-message write-message #t))
    (syscaller-free-thread
     (lambda ()
       (dynamic-wind
         void
         (lambda ()
           (let lp ()
             (sync
              ;; If we shutdown, we won't loop
              shutdown-time
              ;; Otherwise, if a new connection is ready, let's go
              (handle-evt ocapn-sock-listener
                          (lambda _
                            (handle-ocapn-sock-listen)
                            (lp))))))
         do-cleanup))))

  (define base-beh
    (methods
     [(netlayer-name) 'onion]
     [(our-location) our-location]))

  ;; State of the netlayer before it gets called with 'setup
  (define pre-setup-beh
    (methods
     #:extends base-beh
     ;; The machine is now wiring us up with the appropriate behavior for
     ;; when a new connection comes in
     [(setup conn-establisher)
      (start-listen-thread conn-establisher)
      ;; Now that we're set up, transition to the main behavior
      (bcom (ready-beh conn-establisher))]))
  (define (ready-beh conn-establisher)
    (methods
     #:extends base-beh
     [(self-location? loc)
      (same-machine-location? our-location loc)]
     [(connect-to remote-machine)
      (match remote-machine
        [(ocapn-machine 'onion address #f)
         ;; hacky way to start the connection in another thread but with
         ;; working promise machinery
         (define connect-vat (make-vat))
         (define (^start-conn _bcom)
           (lambda ()
             (define-values (ip op)
               (unix-socket-connect tor-socks-path))
             (onion-socks5-setup! ip op (string-append address ".onion"))
             (define-values (read-message write-message)
               (read-write-procs ip op))
             (<- conn-establisher read-message write-message #f)))
         (define start-conn (connect-vat 'spawn ^start-conn))
         (<- start-conn)])]))
  pre-setup-beh)

(define (_finish-setup-onion private-key service-id tor-socks-path
                             ocapn-tmp-dir ocapn-sock-path ocapn-sock-listener)
  ;; TODO: Cleanup tor subprocess also.
  (define (do-cleanup)
    (unix-socket-close-listener ocapn-sock-listener)
    (delete-file ocapn-sock-path)
    (delete-directory ocapn-tmp-dir))

  (define our-location
    (ocapn-machine 'onion service-id #f))

  (values (spawn ^onion-netlayer our-location ocapn-sock-listener
                 tor-socks-path do-cleanup)
          private-key service-id))

;; TODO: I guess this really *should* return one value to its
;; continuation, and the pre-setup-beh above should also support
;; our-location
(define (new-onion-netlayer
         #:tor-control-path [tor-control-path default-tor-control-path]
         #:tor-socks-path [tor-socks-path default-tor-socks-path]
         #:tor-ocapn-socks-dir [tor-ocapn-socks-dir default-tor-ocapn-socks-dir])
  (define-values (ocapn-tmp-dir ocapn-sock-path ocapn-sock-listener service-id private-key)
    (new-tor-connection tor-control-path tor-ocapn-socks-dir))

  (_finish-setup-onion private-key service-id tor-socks-path
                       ocapn-tmp-dir ocapn-sock-path ocapn-sock-listener))

(define (restore-onion-netlayer
         service-id private-key
         #:tor-control-path [tor-control-path default-tor-control-path]
         #:tor-socks-path [tor-socks-path default-tor-socks-path]
         #:tor-ocapn-socks-dir [tor-ocapn-socks-dir default-tor-ocapn-socks-dir])
  (define-values (ocapn-tmp-dir ocapn-sock-path ocapn-sock-listener)
    (restore-tor-connection tor-control-path tor-ocapn-socks-dir
                            private-key service-id))
  (_finish-setup-onion private-key service-id tor-socks-path
                       ocapn-tmp-dir ocapn-sock-path ocapn-sock-listener))


